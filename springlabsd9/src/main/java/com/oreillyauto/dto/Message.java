package com.oreillyauto.dto;

public class Message {
	private String messageType;
	private String message;
	
	public Message() {
		super();
	}

	public Message(String messageType, String message) {
		super();
		this.messageType = messageType;
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
